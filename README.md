# Hershey Text for Inkscape

An Inkscape extension for rendering stroke-font text in Inkscape, using SVG fonts.

Introduction and user guide available at: https://wiki.evilmadscientist.com/Hershey_Text

Currently compatible (and included) with Inkscape 1.2

-----

A version for Inkscape <1.0 is maintained in a [separate branch](https://gitlab.com/oskay/hershey-text/-/tree/Inkscape_0.9x).

-----

#### Author: 

Windell H. Oskay, Evil Mad Scientist Laboratories

https://shop.evilmadscientist.com

-----

#### CLI Test:

python hershey.py  test_doc.svg > test_out.svg

-----

#### Software license:

GPL v2

-----

#### Font license information

The classic Hershey fonts included are derived from
work by Dr. A. V. Hershey.

Additional modern "EMS" fonts in this distribution are
derivatives created from fonts licensed under the SIL
Open Font License.

For full credits and license information, please read the
credits embedded within the SVG fonts included with this
distribution.